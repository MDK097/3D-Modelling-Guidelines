# Reference Images
[![](http://img.youtube.com/vi/j0yyjfzpKE8/0.jpg)](http://www.youtube.com/watch?v=j0yyjfzpKE8 "")
# Cutting holes in meshes
[![](http://img.youtube.com/vi/Ci1jBOm_5NY/0.jpg)](http://www.youtube.com/watch?v=Ci1jBOm_5NY "")
# Adding ridges to meshes
[![](http://img.youtube.com/vi/64AjXYLRbfw/0.jpg)](http://www.youtube.com/watch?v=64AjXYLRbfw "")
# Adding bevels
[![](http://img.youtube.com/vi/JSvGts95S7A/0.jpg)](http://www.youtube.com/watch?v=JSvGts95S7A "")
