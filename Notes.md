# Basic concepts
## Introduction
## Meshes
## Transform
### Translate
### Rotate
### Scale
## Camera: Orthogonal vs Perspective
## Reference Image
# Blender tutorial
## Basic tools
### Extrusion
### Modifiers
